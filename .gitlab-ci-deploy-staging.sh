#!/bin/bash

set -xe

# Connect to the staging VM through SSH and execute the following steps:
# - login into the GitLab Container Registry
# - remove a previous running container (if exists)
# - pull the latest image
# - run the app container
# - exit
ssh -o PreferredAuthentications=publickey $AWS_VM_USER@$AWS_VM_URL \
"docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY \
&& docker rm --force hellopipelinec || true \
&& docker pull $CONTAINER_IMAGE:latest \
&& docker run -d --name hellopipelinec -p 8080:8080 --link mysql $CONTAINER_IMAGE:latest \
&& exit"



