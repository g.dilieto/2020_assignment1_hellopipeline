package hellopipeline.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import hellopipeline.model.Signature;

@Repository
public interface SignatureRepository extends CrudRepository<Signature, Integer> {

	List<Signature> findAllByOrderByIdAsc();
}