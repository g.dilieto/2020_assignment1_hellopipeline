package hellopipeline;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;

@SpringBootApplication
public class HellopipelineApplication {
	public static void main(String[] args) {
		SpringApplication.run(HellopipelineApplication.class, args);
	}
}