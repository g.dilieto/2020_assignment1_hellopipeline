package hellopipeline.controller;

import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import hellopipeline.model.Signature;
import hellopipeline.repository.SignatureRepository;

@RestController
public class SignatureController {
	
	@Autowired 
	private SignatureRepository signatureRepository;

	@GetMapping("/getSignatures")
	String getSignatures() {
		Iterator<Signature> signatureIterator = signatureRepository.findAllByOrderByIdAsc().iterator();
		StringBuffer signaturesBuffer = new StringBuffer();
		
		while (signatureIterator.hasNext()) {
	        Signature signature = signatureIterator.next();
	        signaturesBuffer.append(signature.getFirstName() + ' ' + signature.getLastName());
	        if (signatureIterator.hasNext()) {
	        	signaturesBuffer.append(", ");
	        } else {
	        	signaturesBuffer.append(".");
	        }
	     }
		
		return "Welcome list: " + signaturesBuffer.toString();
	}
	
	@GetMapping("/addSignature")
	String addSignature(@RequestParam String firstName, @RequestParam String lastName) {
		Signature newSignature = new Signature();
		newSignature.setFirstName(firstName);
		newSignature.setLastName(lastName);
	    signatureRepository.save(newSignature);
		return "Hi " + firstName + " " + lastName + ", we've just added you to the welcome list!";
	}

}
