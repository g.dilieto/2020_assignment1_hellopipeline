package hellopipeline.integration;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import hellopipeline.categories.IntegrationTest;

@RunWith(SpringRunner.class)
@SpringBootTest()
@ActiveProfiles("integration-test")
@Category(IntegrationTest.class)
public class HellopipelineApplicationIntegrationTests {

	@Test
	public void contextLoads() {
	}
	
}
