package hellopipeline.integration.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;

import hellopipeline.categories.IntegrationTest;
import hellopipeline.model.Signature;
import hellopipeline.repository.SignatureRepository;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration-test")
@Category(IntegrationTest.class)
public class SignatureControllerIntegrationTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private SignatureRepository signatureRepository;

	private List<Signature> signatures;
	
	@Before
	public void setUp() {
        this.signatures = new ArrayList<>();
        this.signatures.add(new Signature("Pino", "Occhio"));
        this.signatures.add(new Signature("Topo", "Lino"));

		this.signatureRepository.deleteAll();
	}

	@Test
	public void addSignatureResponse() {
		String body;
		
		body = this.restTemplate.getForObject("/addSignature?firstName=" + this.signatures.get(0).getFirstName() + "&lastName=" + this.signatures.get(0).getLastName(), String.class);
		assertThat(body).isEqualTo("Hi " + this.signatures.get(0).getFirstName() + " " + this.signatures.get(0).getLastName() + ", we've just added you to the welcome list!");
		
		body = this.restTemplate.getForObject("/addSignature?firstName=" + this.signatures.get(1).getFirstName() + "&lastName=" + this.signatures.get(1).getLastName(), String.class);
		assertThat(body).isEqualTo("Hi " + this.signatures.get(1).getFirstName() + " " + this.signatures.get(1).getLastName() + ", we've just added you to the welcome list!");
	}
	
	@Test
	public void getSignaturesResponse() {
		String body;
		
		body = this.restTemplate.getForObject("/getSignatures", String.class);
		assertThat(body).isEqualTo(
			"Welcome list: "
		);

		this.restTemplate.getForObject("/addSignature?firstName=" + this.signatures.get(0).getFirstName() + "&lastName=" + this.signatures.get(0).getLastName(), String.class);
		
		body = this.restTemplate.getForObject("/getSignatures", String.class);
		assertThat(body).isEqualTo(
			"Welcome list: " 
			+ this.signatures.get(0).getFirstName()  + " " + this.signatures.get(0).getLastName()  + "."
		);
		

		this.restTemplate.getForObject("/addSignature?firstName=" + this.signatures.get(1).getFirstName() + "&lastName=" + this.signatures.get(1).getLastName(), String.class);
		
		body = this.restTemplate.getForObject("/getSignatures", String.class);
		assertThat(body).isEqualTo(
			"Welcome list: " 
			+ this.signatures.get(0).getFirstName() + " " + this.signatures.get(0).getLastName() + ", " 
			+ this.signatures.get(1).getFirstName() + " " + this.signatures.get(1).getLastName() + "."
		);
	}
}
