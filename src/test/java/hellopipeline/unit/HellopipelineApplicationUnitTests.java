package hellopipeline.unit;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import hellopipeline.categories.UnitTest;
import hellopipeline.repository.SignatureRepository;

@RunWith(SpringRunner.class)
@SpringBootTest()
@ActiveProfiles("unit-test")
@Category(UnitTest.class)
public class HellopipelineApplicationUnitTests {

	@MockBean
	public SignatureRepository signatureRepository;
	
	@Test
	public void contextLoads() {
	}

}
