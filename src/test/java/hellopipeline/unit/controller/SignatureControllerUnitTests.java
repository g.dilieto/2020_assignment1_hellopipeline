package hellopipeline.unit.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import org.mockito.ArgumentMatcher;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import static org.mockito.ArgumentMatchers.argThat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;

import hellopipeline.categories.UnitTest;
import hellopipeline.model.Signature;
import hellopipeline.repository.SignatureRepository;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("unit-test")
@Category(UnitTest.class)
public class SignatureControllerUnitTests {
	
	@MockBean
	public SignatureRepository signatureRepository;

	@Autowired
	private TestRestTemplate restTemplate;

	private List<Signature> signatures;

	@Before
	public void setUp() throws Exception {
		this.signatures = Arrays.asList(new Signature[]{, });

        this.signatures = new ArrayList<>();
        this.signatures.add(new Signature("Pino", "Occhio"));
        this.signatures.add(new Signature("Topo", "Lino"));

		MockitoAnnotations.initMocks(this);
		
		Mockito.doReturn(signatures.get(0)).when(this.signatureRepository).save(argThat(new SignatureMatcher(signatures.get(0))));
		Mockito.doReturn(signatures.get(1)).when(this.signatureRepository).save(argThat(new SignatureMatcher(signatures.get(1))));

		Mockito.doReturn(signatures).when(this.signatureRepository).findAllByOrderByIdAsc();
	}

	@Test
	public void addSignatureResponse() throws Exception {
		
		String body;
		
		body = this.restTemplate.getForObject("/addSignature?firstName=" + this.signatures.get(0).getFirstName() + "&lastName=" + this.signatures.get(0).getLastName(), String.class);
		assertThat(body).isEqualTo("Hi " + this.signatures.get(0).getFirstName() + " " + this.signatures.get(0).getLastName() + ", we've just added you to the welcome list!");
		
		body = this.restTemplate.getForObject("/addSignature?firstName=" + this.signatures.get(1).getFirstName() + "&lastName=" + this.signatures.get(1).getLastName(), String.class);
		assertThat(body).isEqualTo("Hi " + this.signatures.get(1).getFirstName() + " " + this.signatures.get(1).getLastName() + ", we've just added you to the welcome list!");
	}

	@Test
	public void getSignaturesResponse() throws Exception {

		String body;

		body = this.restTemplate.getForObject("/getSignatures", String.class);
		assertThat(body).isEqualTo(
			"Welcome list: " 
			+ this.signatures.get(0).getFirstName() + " " + this.signatures.get(0).getLastName() + ", " 
			+ this.signatures.get(1).getFirstName() + " " + this.signatures.get(1).getLastName() + "."
		);
	}

}

class SignatureMatcher implements ArgumentMatcher<Signature> {

	private Signature left;

	SignatureMatcher(Signature signature) {
		this.left = signature;
	}

	@Override
	public boolean matches(Signature right) {
		return left.getFirstName().equals(right.getFirstName()) && left.getLastName().equals(right.getLastName());
	}
}
