FROM openjdk:8-jdk-slim

WORKDIR /serve/app

COPY ./target/hellopipeline.jar /serve/app

ENV PORT 8080
EXPOSE $PORT
CMD ["java", "-jar", "hellopipeline.jar"]