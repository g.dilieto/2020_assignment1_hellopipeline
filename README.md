# Hellopipeline
**Hellopipeline** is a simple web application developed with <a href="https://spring.io/projects/spring-framework">
    <img width=100 src="https://spring.io/images/spring-logo-9146a4d3298760c2e7e49595184e1975.svg" alt="Spring framework">
</a> framework.  
The web app records the signature of a user and adds it to the **welcome list**. 
# Repository
Here is the [GitLab repository](https://gitlab.com/g.dilieto/2020_assignment1_hellopipeline)
# Deployment Overview
The project is divided into 2 components: 
- the **Java/Spring application** which records and displays the signatures in the welcome list;
- the external **Mysql service** which stores the data permanently;
![image deployment-diagram](imgs/deployment-diagram.png)  
The deployment of the application was carried out thanks to the services of amazon **AWS**, in particular an Ubuntu Virtual Machine was configured to host the containers for Mysql and the web app.  
The VM, reachable through the addresses described below, exposes port 8080 through which it is possible to connect to the web app.  
The **public ip** to access the virtual machine is the follow: 3.19.228.116.  
Or alternatively the **url**: ec2-3-19-228-116.us-east-2.compute.amazonaws.com.
# Usage
In order to **add a new signature** you can make a GET request to the web app:
```https
http://3.19.228.116:8080/addSignature?firstName=[FIRSTNAME]&lastName=[LASTNAME]
```
Otherwise, to **view the welcome list**:
```https
http://3.19.228.116:8080/getSignatures
```
## Adding a signature
Before adding a signature we have this welcome list:  

![image welcomeList](imgs/welcomeList.png)  

We can add a signature (for example "Mario Rossi") this way:    

![image welcomeList_2](imgs/welcomeList_2.png)  

"Mario Rossi" is now part of the welcome list:  

![image welcomeList_3](imgs/welcomeList_3.png)  
# Pipeline 
The **Hellopipeline** project of the course allowed us to use continuous software development methodologies to minimize the possibility of introducing errors during application development, which require no or minimal human intervention, from the development of new code to its deployment.

In particular, the pipeline developed in this project, in accordance with the **GitLab CI/CD tools**, allowed us to apply the continuous methods through the configuration of the .gitlab-ci.yml file.  
The pileline is divided into **7 stages**: build, verify, unit-test, integration-test, package, release and deploy.  
Some of these stages run jobs in parallel, in particular the verify stage allows you to run two jobs in parallel (lint and sats), but also the package stage (package and get-version).  
All the stages are described in detail below.
## Stages
#### build
Use maven to compile the project source code.

#### verify
The verify stage runs two jobs in parallel: lint and sats.  
The **lint** job runs maven's checkstyle plugin, which perform a static code check in order to find class design problems or method design problems. Furthermore, for educational reasons related to this specific project, we have decided to allow the failure of this job. However, it is possible to view the artifacts produced by the failure of this job, in particular the checkstyle-result.xml file presents the result of the checkstyle execution.  
The **sats** job runs the maven spotbugs plugin which uses static analysis to look for bugs in Java code.

#### unit-test
Unit tests are conducted to verify the correctness of the application's controller behavior in adding signatures to the welcome list.  
The tests are performed using **mockito**, which allows you to use to mock the repository of Mysql.  
The unit-test stage is executed with maven test, activating the "unit-test" profile in Spring.  
This profile configures surefire plugin to run only test cases with the junit Category 'UnitTest'.

#### integration-test
The integration tests are carried out by making the spring application interact with the Mysql service and verifying that the two components allows to add and collect user signatures.  
The integration-test is executed with maven test, activating the "integration-test" profile in Spring.  
This profile configures surefire plugin to run only test cases with the junit Category 'IntegrationTest'.  
It also allows to connect with the "hellopipeline_test" schema instead of the production schema.

#### package
The package stage creates the 'hellopipeline.jar' file which contains Spring compiled app. This step enables the production profile in spring in order to use the production schema that points to Mysql container in the AWS Virtual Machine.  
The package stage also outputs an artifact containing the 'pom.xml' project version.

#### release
The release stage builds the docker image that contains the Spring application.  
Each time the release stage is executed, a new image is built with the project version obtained from the package stage.
The image is then pushed to the GitLab Container Registry, and an image tagged 'latest' is updated every time to align to the last version available.

#### deploy
The deploy stage deploys the Spring Boot app with SSH to amazon AWS Ubuntu Virtual Machine, loading the docker image pushed to the docker registry in the release stage.

## Organizational choices
#### Dividing the stages according to the branch
To simulate a more realistic case, the execution of the stages has been divided according to the branch in which the commit is made:  
* commit on **master**: package, release and deploy stages are performed.  
* commit on **develop**: integration-test stages are performed.  
* commit on **feature**: the build, verify and unit-test stages are performed.
![image branches](imgs/branches.png)

Following the flow: commit a change on **feature** -> merge of the change in **develop** -> merge of the change in **master**, the entire pipeline runs.
This step forced us to reconsider the [caching system of the pipeline](#cashing-of-the-pipeline).

#### Spring profiles
The profiles, in Spring, allow to obtain a particular behavior, when they are activated.  
For example to select a particular **application-[profile].properties** file depending on the context the application is in.  
In our case we have 4 profiles in addition to the default:
* **development** is useful when developing a new feature, in particular, when interacting with a locally installed Mysql instance.  
* **unit-test** is useful during the unit-test stage as it allows the application to start unit tests using mokito.  
* **integration-test** is useful during the integration-test stage, as it allows the application to connect to the Mysql instance with a specific configuration.
* **production** it is useful to specify the configuration suitable for the production environment, in this case it allows the application to connect to the Mysql container started on the VM.
## Problems encountered
#### Versioning of the release
We wanted to use the project **version** present in the 'pom.xml' file as a **tag** for the image uploaded to the GitLab registry.  
Unfortunately there isn't an easy way to set a 'variable' during the job execution (package), and read that 'variable' in another job (release).  
Our solution consists of declaring another job (get-version) that outputs an artifact that is a source file which export a variable containing the version, in order to use that variable in the release job.  
Suppose that we have this initial state with the latest image equal to the image tagged with version 1.0.0:
![image version1](imgs/version.png)
Let's change the pom version to the 2.0.0 and execute the entire pipeline:
![image version2](imgs/version_2.png)
Now the latest version is syncronized with the 2.0.0 version.

#### Linking the containers
In the deploy stage we decided to use '--link mysql' to allow the container running the app to connect with the container running Mysql. Even though we know that a more proper way to operate is to create a network and run the containers in the same network, we decided to use '--link mysql' because 'mysql' it is the only container that the application needs to connect with.  
![image link](imgs/link.png)

#### Cashing of the pipeline
Since we decided to [divide the stages according to different branches](#dividing-the-stages-according-to-the-branch), delivering a change to the master branch involves 3 commit (one for the feature, one for the merge of the feature branch inside the develop branch, one for the merge of the develop branch inside the master branch). And using the branch name (with '$CI_COMMIT_REF_NAME' as a key) didn't allow us to share the maven repository accross the jobs executed for the different commits.  
That's why we decided to use a static 'key' for the cache declared globally in the '.gitlab-ci.yml' file.  
![image cache](imgs/cache.png)  
This way the cache is shared for the whole workflow, even though three pipelines are executed for three different branches.

## Contributing
Gabriele Di Lieto: g.dilieto@campus.unimib.it  
Marco Poveromo: m.poveromo@campus.unimib.it